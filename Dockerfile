FROM armhf/alpine
MAINTAINER dietrich@dbopp.net

COPY qemu-arm-static /usr/bin/qemu-arm-static

RUN ["/usr/bin/qemu-arm-static", "apk",  "update"]
RUN ["/usr/bin/qemu-arm-static", "apk",  "add ca-certificates wget openssl"]

RUN mkdir -p /opt/youtrack/data /opt/youtrack/backup /opt/youtrack/bin

WORKDIR /opt/youtrack

RUN wget \
 https://download.jetbrains.com/charisma/youtrack-2017.4.38399.jar -O /opt/youtrack/bin/youtrack.jar

EXPOSE 80/tcp

VOLUME ["/opt/youtrack/data/", "/opt/youtrack/backup/"]

